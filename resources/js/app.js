require('./bootstrap');

$(document).ready(function(){
    $('.slider').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        // autoplay: true,
        autoplaySpeed: 3000,
        dots: true,
        infinite: true,
        arrows: false,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                centerMode: false,
                centerPadding: '40px',
                slidesToShow: 1
              }
            },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     arrows: false,
            //     centerMode: true,
            //     centerPadding: '40px',
            //     slidesToShow: 1
            //   }
            // }
          ]
    });

    $('.product-list').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
    });

    $('.product-wrapper').hover(function() {
      let $this = $(this);

      $this.toggleClass('product-info-show');
    }, function() {
      let $this = $(this);

      $this.toggleClass('product-info-show');
    })
  });