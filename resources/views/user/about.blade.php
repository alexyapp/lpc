@extends('layouts.app')

@section('content')
    {{-- <div class="row about-the-company">
        <div class="col">
            <div class="row h-100 align-items-center">
                <div class="col h-75 d-flex align-items-center left">

                </div>
                <div class="col h-75 d-flex align-items-center right">
                    <div class="w-75">
                        <h1 class="display-1 text-right libel-suit">
                            ABOUT THE <br> COMPANY
                        </h1>

                        <hr>

                        <p class="text-right helvetica">
                            Management is often the difference between an average and successful service delivery. LPC has a senior management team that has the qualifications, industry experience and energy to manage all aspects of your packaging solution requirements.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row about-the-company">
        <div class="col">
            <div class="row h-100 align-items-center px-lg-5">
                <div class="col h-75 d-none d-lg-flex align-items-center left">

                </div>
                <div class="col h-75 d-flex align-items-center right pr-lg-5">
                    <div>
                        <h1 class="responsive-text text-center text-lg-right libel-suit">
                            ABOUT THE COMPANY
                        </h1>

                        <hr>

                        <p class="text-center text-lg-right">
                            Management is often the difference between an average and successful service delivery. LPC has a senior management team that has the qualifications, industry experience and energy to manage all aspects of your packaging solution requirements.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="row more-info">
        <div class="col d-flex align-items-center justify-content-center">
            <div class="text-center w-50">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-1.png') }}" alt="">
                </div>

                <p class="helvetica">
                    Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                </p>

                <a href="" class="btn btn-secondary helvetica">Read</a>
            </div>
        </div>
        <div class="col d-flex align-items-center justify-content-center">
            <div class="text-center w-50">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-2.png') }}" alt="">
                </div>

                <p class="helvetica">
                    Over the years we have developed relationships with a diverse range of clients. Some of our key clients who have entrusted us to provide them with quality food and beverage liquid packaging solutions are Profoods, Camel, Aquapak, Bohol IQ, IPI, Oro IQ, and many more.
                </p>

                <a href="" class="btn btn-secondary helvetica">Read</a>
            </div>
        </div>
        <div class="col d-flex align-items-center justify-content-center">
            <div class="text-center w-50">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-3.png') }}" alt="">
                </div>

                <p class="helvetica">
                    LIQUID PACKAGING SOLUTIONS is committed to provide quality products thereby meeting all interested parties needs and expectations, compliance to all applicable regulatory and statutory requirements whilst achieving high efficiency, long-term sustainability, and 
profitability within the organization.
                </p>

                <a href="" class="btn btn-secondary helvetica">Read</a>
            </div>
        </div>
    </div> --}}

    <div class="row more-info py-xl-5">
        <div class="col-xl-4 d-flex align-items-center justify-content-center mt-5 mt-xl-0 mb-5 mb-xl-0">
            <div class="text-center w-75">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-1.png') }}" alt="">
                </div>

                <div class="more-info-text-container">
                    <p>
                        Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                    </p>
                </div>

                <a href="" class="btn btn-secondary">Read</a>
            </div>
        </div>
        <div class="col-xl-4 d-flex align-items-center justify-content-center mb-5 mb-xl-0">
            <div class="text-center w-75">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-2.png') }}" alt="">
                </div>

                <div class="more-info-text-container">
                    <p class="helvetica">
                        Over the years we have developed relationships with a diverse range of clients. Some of our key clients who have entrusted us to provide them with quality food and beverage liquid packaging solutions are Profoods, Camel, Aquapak, Bohol IQ, IPI, Oro IQ, and many more.
                    </p>
                </div>

                <a href="" class="btn btn-secondary helvetica">Read</a>
            </div>
        </div>
        <div class="col-xl-4 d-flex align-items-center justify-content-center mb-5 mb-xl-0">
            <div class="text-center w-75">
                <div class="d-flex align-items-center justify-content-center mb-3 icon-container">
                    <img src="{{ asset('images/icon-3.png') }}" alt="">
                </div>

                <div class="more-info-text-container">
                    <p class="helvetica">
                        LIQUID PACKAGING SOLUTIONS is committed to provide quality products thereby meeting all interested parties needs and expectations, compliance to all applicable regulatory and statutory requirements whilst achieving high efficiency, long-term sustainability, and 
    profitability within the organization.
                    </p>
                </div>

                <a href="" class="btn btn-secondary helvetica">Read</a>
            </div>
        </div>
    </div>

    {{-- <div class="row our-clients-and-partners">
        <div class="col d-flex justify-content-center align-items-center">
            <div class="w-75">
                <div class="mb-4">
                    <h1 class="display-1 text-center libel-suit">our clients and partners</h1>
                </div>

                <div>
                    <h5 class="text-center helvetica-bold">
                        Over the years we have developed relationships with a diverse range of clients. Some of our key clients
who have entrusted us to provide them with quality food and beverage liquid packaging solutions are
Profoods, Camel, Aquapak, Bohol H-IQ, IPI, Oro HI-Q, and many more.
                    </h5>
                </div>

                <div>
                    <div class="row">
                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/ns.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/IPI.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/Uniflo.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/marasa-trading.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/Camel.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/Multi-stage.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/Profood.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img src="{{ asset('images/water-source.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}



    <div class="row our-clients-and-partners position-relative py-4 py-lg-5">
        <div class="col d-flex justify-content-center align-items-center">
            <div class="w-75">
                <div>
                    <h1 class="responsive-text text-center libel-suit">our clients and partners</h1>
                </div>

                <div>
                    <h5 class="text-center helvetica-bold">
                        Over the years we have developed relationships with a diverse range of clients. Some of our key clients
who have entrusted us to provide them with quality food and beverage liquid packaging solutions are
Profoods, Camel, Aquapak, Bohol H-IQ, IPI, Oro HI-Q, and many more.
                    </h5>
                </div>

                <div>
                    <div class="row">
                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/ns.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/IPI.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/Uniflo.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/marasa-trading.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/Camel.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-4 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/Multi-stage.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/Profood.png') }}" alt="">
                            </div>
                        </div>

                        <div class="col-6 mb-lg-0 col-lg-3 d-flex align-items-center justify-content-center">
                            <div>
                                <img class="img-fluid" src="{{ asset('images/water-source.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row company-background p-5 justify-content-xl-center">
        <div class="col-12">
            <h1 class="responsive-text nature-spring-blue text-center libel-suit">company background</h1>
        </div>

        <div class="col-12 col-lg-6 col-xl-4 p-lg-5 left">
            <div class="py-4 w-100 w-lg-75 ml-auto">
                <p class="helvetica">
                    LPC was established on October 1, 1997 to cater to our mother company’s growing demand for plastic bottles; Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                </p>
    
                <p class="helvetica">
                    The Company has taken its roots supporting the goal of Philippine Spring Water Resources, Inc., makers of Nature’s Spring bottled water, which is to provide for the public’s basic need of safe drinking water. Since then, it has gradually moved forward and invested in machineries, comprehensive plans and strategies, applications and systems (i.e. SAP), and competent employees to improve its manufacturing processes, and thus to give our mother company the service that it deserves…for eighteen (18) remarkable years.
                </p>
    
                <p class="helvetica">
                    As the market for PET bottles and other plastic food and beverage packaging continues to become even more promising, opportunities for expansion and diversification have opened for LPC to thrive. With this, accompanied by the new strategic directions from our visionary and rather dynamic President, Mr. Daniel Ryan Lua, LPC has begun serving other companies and businesses as well. Indeed, it has widen its client base, and that expanded its operations throughout the whole Philippines by opening branches in Bulacan to cater Luzon and Greater Manila Area clients; and reinforcing the strength and reach of our Head Office in Cebu, including our distribution networks in Cagayan De Oro, Bacolod, Ilo-ilo, Dumaguete, Davao and Tacloban to cater to our Visayas and Mindanao clients. 
                </p>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-4 p-lg-5 right">
            <div class="py-4 w-100 w-lg-75">
                <p class="helvetica">
                    Adapting to the changing and high demands of the market and upholding its competitive position in the
    industry, Liquid Packaging Corporation with its management and employees will continue to bring
    forth our best effort to be the supplier of choice and exceed customer expectations with our prompt
    service- at the right time, every time…
                </p>
    
                <p class="helvetica">
                    With more than 30 high end machines with a high capacity to produce plastics injection products and
    blowing bottles of automated machines manufactured in Germany, Korea, and Taiwan. The machines
    are able to produce plastic caps and bottles mostly intended for food product and beverage packaging.
                </p>
    
                <p class="helvetica">
                    Liquid Packaging Corporation employs consummate people to satisfy the need of our customers and
    produce a high quality of products. The company has envisioned being top on the market providing
    quality pet bottles in different sizes and designs including the customization of products.
                </p>
            </div>
        </div>
    </div>

    {{-- <div class="row company-background p-5">
        <div class="col-12">
            <h1 class="display-1 text-center text-light-blue libel-suit">company background</h1>
        </div>

        <div class="col left">
            <div class="p-5 w-75 ml-auto">
                <p class="helvetica">
                    LPC was established on October 1, 1997 to cater to our mother company’s growing demand for plastic bottles; Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                </p>
    
                <p class="helvetica">
                    The Company has taken its roots supporting the goal of Philippine Spring Water Resources, Inc., makers of Nature’s Spring bottled water, which is to provide for the public’s basic need of safe drinking water. Since then, it has gradually moved forward and invested in machineries, comprehensive plans and strategies, applications and systems (i.e. SAP), and competent employees to improve its manufacturing processes, and thus to give our mother company the service that it deserves…for eighteen (18) remarkable years.
                </p>
    
                <p class="helvetica">
                    As the market for PET bottles and other plastic food and beverage packaging continues to become even more promising, opportunities for expansion and diversification have opened for LPC to thrive. With this, accompanied by the new strategic directions from our visionary and rather dynamic President, Mr. Daniel Ryan Lua, LPC has begun serving other companies and businesses as well. Indeed, it has widen its client base, and that expanded its operations throughout the whole Philippines by opening branches in Bulacan to cater Luzon and Greater Manila Area clients; and reinforcing the strength and reach of our Head Office in Cebu, including our distribution networks in Cagayan De Oro, Bacolod, Ilo-ilo, Dumaguete, Davao and Tacloban to cater to our Visayas and Mindanao clients. 
                </p>
            </div>
        </div>
        <div class="col right">
            <div class="p-5 w-75">
                <p class="helvetica">
                    Adapting to the changing and high demands of the market and upholding its competitive position in the
    industry, Liquid Packaging Corporation with its management and employees will continue to bring
    forth our best effort to be the supplier of choice and exceed customer expectations with our prompt
    service- at the right time, every time…
                </p>
    
                <p class="helvetica">
                    With more than 30 high end machines with a high capacity to produce plastics injection products and
    blowing bottles of automated machines manufactured in Germany, Korea, and Taiwan. The machines
    are able to produce plastic caps and bottles mostly intended for food product and beverage packaging.
                </p>
    
                <p class="helvetica">
                    Liquid Packaging Corporation employs consummate people to satisfy the need of our customers and
    produce a high quality of products. The company has envisioned being top on the market providing
    quality pet bottles in different sizes and designs including the customization of products.
                </p>
            </div>
        </div>
    </div> --}}

    <div class="row quality-assurance p-5 bg-light-blue justify-content-xl-center">
        <div class="col-12 col-lg-6 col-xl-4 left border-right">
            <div class="py-4 p-lg-5 w-lg-75 ml-auto">
                <div class="mb-3 mb-lg-5">
                    <h1 class="responsive-text-md nature-spring-blue libel-suit">quality management assurance</h1>
                </div>

                <p class="helvetica">
                    LPC was established on October 1, 1997 to cater to our mother company’s growing demand for plastic bottles; Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                </p>
    
                <p class="helvetica">
                    The Company has taken its roots supporting the goal of Philippine Spring Water Resources, Inc., makers of Nature’s Spring bottled water, which is to provide for the public’s basic need of safe drinking water. Since then, it has gradually moved forward and invested in machineries, comprehensive plans and strategies, applications and systems (i.e. SAP), and competent employees to improve its manufacturing processes, and thus to give our mother company the service that it deserves…for eighteen (18) remarkable years.
                </p>
    
                <p class="helvetica">
                    As the market for PET bottles and other plastic food and beverage packaging continues to become even more promising, opportunities for expansion and diversification have opened for LPC to thrive. With this, accompanied by the new strategic directions from our visionary and rather dynamic President, Mr. Daniel Ryan Lua, LPC has begun serving other companies and businesses as well. Indeed, it has widen its client base, and that expanded its operations throughout the whole Philippines by opening branches in Bulacan to cater Luzon and Greater Manila Area clients; and reinforcing the strength and reach of our Head Office in Cebu, including our distribution networks in Cagayan De Oro, Bacolod, Ilo-ilo, Dumaguete, Davao and Tacloban to cater to our Visayas and Mindanao clients. 
                </p>
            </div>
        </div>
        
        <div class="col-12 col-lg-6 col-xl-4 right">
            <div class="py-4 p-lg-5 w-lg-75">
                <div class="mb-3 mb-lg-5">
                    <h1 class="responsive-text-md nature-spring-blue libel-suit">quality policy</h1>
                </div>

                <p class="helvetica">
                    Adapting to the changing and high demands of the market and upholding its competitive position in the
    industry, Liquid Packaging Corporation with its management and employees will continue to bring
    forth our best effort to be the supplier of choice and exceed customer expectations with our prompt
    service- at the right time, every time…
                </p>
    
                <p class="helvetica">
                    With more than 30 high end machines with a high capacity to produce plastics injection products and
    blowing bottles of automated machines manufactured in Germany, Korea, and Taiwan. The machines
    are able to produce plastic caps and bottles mostly intended for food product and beverage packaging.
                </p>
    
                <p class="helvetica">
                    Liquid Packaging Corporation employs consummate people to satisfy the need of our customers and
    produce a high quality of products. The company has envisioned being top on the market providing
    quality pet bottles in different sizes and designs including the customization of products.
                </p>
            </div>
        </div>
    </div>

    {{-- <div class="row quality-assurance p-5">
        <div class="col left">
            <div class="p-5 w-75 ml-auto">
                <div class="mb-3">
                    <h1 class="display-1 libel-suit">quality management assurance</h1>
                </div>

                <p class="helvetica">
                    LPC was established on October 1, 1997 to cater to our mother company’s growing demand for plastic bottles; Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                </p>
    
                <p class="helvetica">
                    The Company has taken its roots supporting the goal of Philippine Spring Water Resources, Inc., makers of Nature’s Spring bottled water, which is to provide for the public’s basic need of safe drinking water. Since then, it has gradually moved forward and invested in machineries, comprehensive plans and strategies, applications and systems (i.e. SAP), and competent employees to improve its manufacturing processes, and thus to give our mother company the service that it deserves…for eighteen (18) remarkable years.
                </p>
    
                <p class="helvetica">
                    As the market for PET bottles and other plastic food and beverage packaging continues to become even more promising, opportunities for expansion and diversification have opened for LPC to thrive. With this, accompanied by the new strategic directions from our visionary and rather dynamic President, Mr. Daniel Ryan Lua, LPC has begun serving other companies and businesses as well. Indeed, it has widen its client base, and that expanded its operations throughout the whole Philippines by opening branches in Bulacan to cater Luzon and Greater Manila Area clients; and reinforcing the strength and reach of our Head Office in Cebu, including our distribution networks in Cagayan De Oro, Bacolod, Ilo-ilo, Dumaguete, Davao and Tacloban to cater to our Visayas and Mindanao clients. 
                </p>
            </div>
        </div>
        <div class="col right">
            <div class="p-5 w-75">
                <div class="mb-3">
                    <h1 class="display-1 libel-suit">quality policy</h1>
                </div>

                <p class="helvetica">
                    Adapting to the changing and high demands of the market and upholding its competitive position in the
    industry, Liquid Packaging Corporation with its management and employees will continue to bring
    forth our best effort to be the supplier of choice and exceed customer expectations with our prompt
    service- at the right time, every time…
                </p>
    
                <p class="helvetica">
                    With more than 30 high end machines with a high capacity to produce plastics injection products and
    blowing bottles of automated machines manufactured in Germany, Korea, and Taiwan. The machines
    are able to produce plastic caps and bottles mostly intended for food product and beverage packaging.
                </p>
    
                <p class="helvetica">
                    Liquid Packaging Corporation employs consummate people to satisfy the need of our customers and
    produce a high quality of products. The company has envisioned being top on the market providing
    quality pet bottles in different sizes and designs including the customization of products.
                </p>
            </div>
        </div>
    </div> --}}

    @include('partials.footer', ['invert' => true])
@endsection