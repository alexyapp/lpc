@extends('layouts.app')

@section('content')
    <div class="row pt-5" style="background: #5AAACA;">
        <div class="col-12">
            <h1 class="responsive-text libel-suit text-white text-center mb-0">CONTACT US</h1>
        </div>
    </div>
    @include('partials.footer', ['includeFooter' => false])

    <div class="row justify-content-lg-center py-4">
        <div class="col-12 px-3 pb-0">
            <div class="row justify-content-lg-center">
                <div class="col-9">
                    <h5 class="display-5 mb-0 helvetica-bold">Philippines Branch Details</h5>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-3 px-3 pt-3">
            <div class="mb-3">
                <p class="mb-0 font-italic font-weight-bold">Cebu Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Latasan, labogon</p>
                <p class="mb-0 helvetica font-weight-bold">Mandaue City, Cebu</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5639</p>
                <p class="mb-0 helvetica">0917-867-5524</p>
                <p class="mb-0 helvetica">(032) 430-4399</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Ilo-ilo Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Brgy. Buntatala, Leganes, Ilo-ilo</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5599</p>
                <p class="mb-0 helvetica">0917-867-5433</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Kabankalan Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Gregoria Village, Phase 5 Brgy. 3 Kabankalan City</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-4468</p>
                <p class="mb-0 helvetica">0917-867-5433</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Bacolod Address:</p>
                <p class="mb-0 helvetica font-weight-bold">H3 Warehouse Bredco Reclamation Area</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5472</p>
                <p class="mb-0 helvetica">0917-867-5717</p>
                <p class="mb-0 helvetica">0917-867-5718</p>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 px-3 pt-3">
            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Bulacan Address:</p>
                <p class="mb-0 helvetica font-weight-bold">FBIC – First Bulacan Industrial City- Blk.2 Lot 16 Glowdel Avenue, Tikay, Malolos Bulacan</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5449</p>
                <p class="mb-0 helvetica">0917-706-7705</p>
                <p class="mb-0 helvetica">(044) 697-6088</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Davao Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Mahayahay, Diversion Road Matina Crossing, Davao City</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5433</p>
                <p class="mb-0 helvetica">0917-867-5743</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Palawan Address:</p>
                <p class="mb-0 helvetica font-weight-bold">PEO Road, Brgy. Bancao-Bancao, Rizal Avenue, Puerto Princesa City, Palawan</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5833</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Zamboanga Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Gov. Ramos Teodoro Lane Sta, Maria Zamboanga City</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5753</p>
                <p class="mb-0 helvetica">0917-867-5472</p>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 px-3 pt-3">
            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Dumaguete Address:</p>
                <p class="mb-0 helvetica font-weight-bold">South Road Banilad, Duplamilco Warehouse Dumaguete City, Negros Oriental</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5727</p>
                <p class="mb-0 helvetica">0936-187-4837</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Tacloban Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Rocksun Warehouse, Brgy.79 Marasbaras Tacloban City, Leyte</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5620</p>
                <p class="mb-0 helvetica">0917-867-5803</p>
            </div>

            <div class="mb-3">
                <p class="mb-0 helvetica font-italic font-weight-bold">Cagayan De Oro Address:</p>
                <p class="mb-0 helvetica font-weight-bold">Kimwa Compound Blay CDO</p>
                <p class="mb-0 helvetica font-italic font-weight-bold">Contact no.</p>
                <p class="mb-0 helvetica">0917-867-5459</p>
                <p class="mb-0 helvetica">0917-867-5480</p>
            </div>
        </div>
    </div>
@endsection