@extends('layouts.app')

@section('content')
    <div class="row home-header">
        <div class="col">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center left">
                    <div class="d-block d-lg-none py-5 py-lg-0">
                        <h1 class="responsive-text libel-suit">LIQUID PACKAGING CORPORATION</h1>
                    </div>
                    <div class="d-none d-lg-block">
                        <h1 class="display-1 libel-suit">LIQUID</h1>
                        <h1 class="display-1 libel-suit">PACKAGING</h1>
                        <h1 class="display-1 libel-suit">CORPORATION</h1>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center right">
                    <div class="px-5">
                        <p class="helvetica text-right">
                            Liquid Packaging Corporation (LPC) is now reaching new heights in its operations as the 
                            Company continues to deliver optimum service to our clients, manufacturing and distribution of quality PET (polyethylene terephthalate) bottles throughout the Philippines.
                        </p>
    
                        <div class="text-center">
                            <a href="{{ route('user.about') }}" class="btn btn-secondary helvetica">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mission-vision">
        <div class="col-12 col-lg-6 vision-container">
            <div class="d-flex flex-column align-items-center justify-content-end h-75">
                <div>
                    <h1 class="responsive-text text-white text-center libel-suit">
                        VISION
                    </h1>
                    <h5 class="vision text-center helvetica-bold-oblique">“We are the market leader of quality packaging solutions.”</h5>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 mission-container">
            <div class="d-flex flex-column align-items-center justify-content-start h-75">
                <div>
                    <h1 class="responsive-text text-white text-center libel-suit">
                        MISSION
                    </h1>
                    <h5 class="mission text-center helvetica-bold-oblique">“We provide excellent customer service.”</h5>

                    <div class="text-center mt-4">
                        <a href="" class="btn btn-secondary helvetica">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="row mission-vision">
        <div class="col-12 col-md-6 d-flex align-items-end left">
            <div class="h-50 w-100">
                <div class="position-relative h-100">
                    <h1 class="display-1 text-white position-absolute libel-suit">
                        VISION
                    </h1>
                    <h5 class="vision position-absolute text-center helvetica-bold-oblique">“We are the market leader of quality packaging solutions.”</h5>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 d-flex align-items-start right">
            <div class="h-50 w-100">
                <div class="position-relative h-100">
                    <h1 class="display-1 text-white position-absolute libel-suit">
                        MISSION
                    </h1>
                    <h5 class="mission position-absolute text-center helvetica-bold-oblique">“We provide excellent customer service.”</h5>
                </div>

                <div class="text-center">
                    <a href="" class="btn btn-secondary helvetica">Learn more</a>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row products">
        <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center left">
            <div>
                <h1 class="responsive-text text-white libel-suit">PRODUCTS</h1>
            </div>
        </div>

        <div class="col-12 col-lg-6 right d-flex align-items-center px-0">
            <div class="text-center h-50 d-flex flex-column justify-content-center w-100 p-5 description">
                <h5 class="helvetica text-right">
                    Our products have pass the quality processes established thru our quality 
                    management system from sourcing of raw materials to its distribution
                </h5>

                <div class="mt-lg-5">
                    <a class="btn btn-secondary helvetica" href="{{ route('user.products') }}">Learn more</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row testimonials my-5 d-none">
        <div class="col d-flex align-items-center justify-content-center left mb-5">
            <div>
                <h1 class="display-1 libel-suit text-light-blue mb-0">TESTIMONIALS</h1>
            </div>
        </div>

        <div class="col-12 px-0">
            <div class="slider">
                <div>
                    <div class="testimonial-container">
                        <div class="testimonial-card p-5 mx-3 text-center position-relative d-flex align-items-center">
                            {{-- <div>
                                <img class="d-block mx-auto" src="{{ asset('images/quote.svg') }}" alt="">
                            </div> --}}
                            
                            <div class="testimonial-content-container">
                                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel facilisis ipsum, id facilisis nisl.</p>
                            </div>
                        
                            <div class="testimonial-user position-absolute">
                                <div class="testimonial-avatar mb-3 shadow">
        
                                </div>

                                <h5>John Doe</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-container">
                        <div class="testimonial-card p-5 mx-3 text-center position-relative d-flex align-items-center">
                            {{-- <div>
                                <img class="d-block mx-auto" src="{{ asset('images/quote.svg') }}" alt="">
                            </div> --}}
                            
                            <div class="testimonial-content-container">
                                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel facilisis ipsum, id facilisis nisl.</p>
                            </div>
                        
                            <div class="testimonial-user position-absolute">
                                <div class="testimonial-avatar mb-3 shadow">
        
                                </div>

                                <h5>John Doe</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-container">
                        <div class="testimonial-card p-5 mx-3 text-center position-relative d-flex align-items-center">
                            {{-- <div>
                                <img class="d-block mx-auto" src="{{ asset('images/quote.svg') }}" alt="">
                            </div> --}}
                            
                            <div class="testimonial-content-container">
                                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel facilisis ipsum, id facilisis nisl.</p>
                            </div>
                        
                            <div class="testimonial-user position-absolute">
                                <div class="testimonial-avatar mb-3 shadow">
        
                                </div>

                                <h5>John Doe</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-container">
                        <div class="testimonial-card p-5 mx-3 text-center position-relative d-flex align-items-center">
                            {{-- <div>
                                <img class="d-block mx-auto" src="{{ asset('images/quote.svg') }}" alt="">
                            </div> --}}
                            
                            <div class="testimonial-content-container">
                                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel facilisis ipsum, id facilisis nisl.</p>
                            </div>
                        
                            <div class="testimonial-user position-absolute">
                                <div class="testimonial-avatar mb-3 shadow">
        
                                </div>

                                <h5>John Doe</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="testimonial-container">
                        <div class="testimonial-card p-5 mx-3 text-center position-relative d-flex align-items-center">
                            {{-- <div>
                                <img class="d-block mx-auto" src="{{ asset('images/quote.svg') }}" alt="">
                            </div> --}}
                            
                            <div class="testimonial-content-container">
                                <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel facilisis ipsum, id facilisis nisl.</p>
                            </div>
                        
                            <div class="testimonial-user position-absolute">
                                <div class="testimonial-avatar mb-3 shadow">
        
                                </div>

                                <h5>John Doe</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')
@endsection