@extends('layouts.app')

@section('content')
    <div class="row faqs-header position-relative justify-content-center">
        <div class="col-9 py-5 faqs-header-content">
            <div>
                <h1 class="responsive-text libel-suit text-light-blue">FAQS</h1>
            </div>
        </div>
    </div>

    <div class="row justify-content-center py-5 faqs">
        <div class="col-lg-4 px-5 border-right left">
            <div class="mb-3">
                <p class="helvetica-bold">What is LPC?</p>

                <p class="text-light-blue font-italic helvetica-bold">
                    Answer:<br>
                    Liquid Packaging Corporation ( LPC), is an ISO 9001:2015 Certified company and an affiliate of Philippine Spring Water Resources, Inc., 
                    (the maker of Nature’s Spring bottled water) has been in the business of manufacturing and distributing of safe and quality food and beverage packaging for twenty one (21) remarkable years already.
                </p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">Do you have delivery?</p>

                <p class="text-light-blue font-italic helvetica-bold">Answer: YES</p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">What is your product line?</p>

                <p class="text-light-blue font-italic helvetica-bold">Answer:</p>
            </div>

            <div class="mb-3">
                <div class="row">
                    <div class="col-12">
                        <p class="helvetica-bold">Where is your location?</p>

                        <p class="text-light-blue font-italic helvetica-bold">Answer:</p>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Cebu Address</p>
                            <p class="mb-0 helvetica-bold">Latasan, Labogon</p>
                            <p class="mb-0 helvetica-bold">Mandaue City, Cebu</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Ilo-ilo Address</p>
                            <p class="mb-0 helvetica-bold">Brgy. Buntatala</p>
                            <p class="mb-0 helvetica-bold">Leganes, Ilo-ilo</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Bulacan Address</p>
                            <p class="mb-0 helvetica-bold">Cagayan De Oro Address:</p>
                            <p class="mb-0 helvetica-bold">FBIC - First Bulacan</p>
                            <p class="mb-0 helvetica-bold">Industrial City</p>
                            <p class="mb-0 helvetica-bold">Blk. 2 Lot 16 Glowdel Avenue</p>
                            <p class="mb-0 helvetica-bold">Tikay, Malolos Bulacan</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Davao Address</p>
                            <p class="mb-0 helvetica-bold">Mahayahay, Diversion Road</p>
                            <p class="mb-0 helvetica-bold">Matina Crossing, Davao City</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Kabankalan Address</p>
                            <p class="mb-0 helvetica-bold">Gregoria Village, Phase 5</p>
                            <p class="mb-0 helvetica-bold">Brgy. 3 Kabankalan City</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Palawan Address</p>
                            <p class="mb-0 helvetica-bold">Bancao Rizal Avenue, Puerto Princesa City, Palawan</p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Dumaguete Address:</p>
                            <p class="mb-0 helvetica-bold">South Road Banilad, Duplamilco Warehouse <br> Dumaguete City, Negros Oriental</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Tacloban Address:</p>
                            <p class="mb-0 helvetica-bold">Rocksun Warehouse, Brgy.79 Marasbaras Tacloban City, Leyte</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Cagayan De Oro Address:</p>
                            <p class="mb-0 helvetica-bold">Kimwa Compound<br>Blay CDO</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Bacolod Address:</p>
                            <p class="mb-0 helvetica-bold">H3 Warehouse Bredco<br>Reclamation Area</p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 helvetica-bold text-light-blue font-italic">Zamboanga Address:</p>
                            <p class="mb-0 helvetica-bold">Gov. Ramos Teodoro<br>Lane Sta, Maria Zamboanga City</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">Will you give discounts for bulk orders?</p>
                <p class="text-light-blue font-italic helvetica-bold">Answer: YES</p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">Did you offer terms and how?</p>
                <p class="text-light-blue font-italic helvetica-bold">Answer: YES WE OFFER TERMS
                    AFTER THREE (3) CONSECUTIVE PURCHASE IN CASH PAYMENT,
                    FILL UP CUSTOMER INFORMATION SHEET.
                    OFFICERS WILL CONDUCT BACKGROUND CHECK FOR APPROVAL.</p>
            </div>
        </div>

        <div class="col-lg-4 px-5">
            <div class="mb-3">
                <p class="helvetica-bold">Do you have Branches Nationwide?</p>
                <p class="text-light-blue font-italic helvetica-bold">
                    Answer:YES <br>
                    Cebu <br> 			 			
                    Dumaguete <br> 
                    Ilo-ilo <br> 					
                    Tacloban <br> 
                    Bulacan <br> 	
                    Cagayan De Oro <br> 
                    Davao <br> 				
                    Bacolod <br> 				
                    Kabankalan <br> 		
                    Zamboanga <br> 	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                    Palawan <br>
                </p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">Do you customize bottles?</p>
                <p class="text-light-blue font-italic helvetica-bold">Answer:YES</p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">What is your lead time in printing bottles?</p>
                <p class="text-light-blue font-italic helvetica-bold">Answer: 3 days</p>
            </div>

            <div class="mb-3">
                <p class="helvetica-bold">What is your contact number?</p>
                <p class="text-light-blue font-italic helvetica-bold">Answer: 3 days</p>

                <div class="row">
                    <div class="col-12">
                        <p class="text-light-blue font-italic helvetica-bold">Answer:</p>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Cebu Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5639 <br> 0917-867-5524 <br> (032) 430-4399
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Ilo-ilo Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5599 <br> 0917-867-5433
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Ilo-ilo Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5599 <br> 0917-867-5433
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Bulacan Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5449 <br> 0917-706-7705 <br> (044) 697-6088
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Davao Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5433 <br> 0917-867-5743
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Kabankalan Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-4468 <br> 0917-867-5433
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Palawan Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-8675833
                            </p>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Dumaguete Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5727 <br> 0936-187-4837
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Tacloban Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5620 <br> 0917-867-5803
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Cagayan De Oro Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5459 <br> 0917-867-5480
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Bacolod Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5472 <br> 0917-867-5717 <br> 0917-867-5718
                            </p>
                        </div>

                        <div class="mb-3">
                            <p class="mb-0 text-light-blue font-italic helvetica-bold">Zamboanga Contact:</p>
                            <p class="text-light-blue helvetica-bold">
                                0917-867-5753 <br> 0917-867-5472
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection