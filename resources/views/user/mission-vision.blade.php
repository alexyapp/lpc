@extends('layouts.app')

@section('content')
    <div class="row align-items-center mission-vision-main">
        <div class="col text-center">
            <h1 class="responsive-text libel-suit">OUR MISSION, VISION AND VALUES</h1>
        </div>
    </div>

    <div class="row value-prop core-competencies text-center p-5 align-items-center">
        <div class="col-12 mb-3">
            <h3 class="responsive-text libel-suit">Our Core Compentencies</h3>
        </div>

        <div class="col">
            <div class="mb-3 icon-container d-flex justify-content-center align-items-center">
                <img src="{{ asset('images/result-driven.png') }}" alt="">
            </div>
            <p class="mb-0 helvetica">Result-driven</p>
        </div>
        
        <div class="col">
            <div class="mb-3 icon-container d-flex justify-content-center align-items-center">
                <img src="{{ asset('images/knowing-the-business.png') }}" alt="">
            </div>
            <p class="mb-0 helvetica">Knowing the Business</p>
        </div>

        <div class="col">
            <div class="mb-3 icon-container d-flex justify-content-center align-items-center">
                <img src="{{ asset('images/communication.png') }}" alt="">
            </div>
            <p class="mb-0 helvetica">Communication</p>
        </div>

        <div class="col">
            <div class="mb-3 icon-container d-flex justify-content-center align-items-center">
                <img src="{{ asset('images/commitment.png') }}" alt="">
            </div>
            <p class="mb-0 helvetica">Commitment</p>
        </div>

        <div class="col">
            <div class="mb-3 icon-container d-flex justify-content-center align-items-center">
                <img src="{{ asset('images/critical-thinking.png') }}" alt="">
            </div>
            <p class="mb-0 helvetica">Critical Thinking</p>
        </div>
    </div>

    <div class="row our-vision-our-mission vh-100 align-items-center text-center">
        <div class="col-12">
            <div>
                <h3 class="responsive-text libel-suit">our vision</h3>
                <h5 class="helvetica-bold">We are the market leader of quality packaging solutions.</h5>
            </div>
        </div>

        <div class="col-12">
            <div>
                <h3 class="responsive-text libel-suit">our mission</h3>
                <h5 class="helvetica-bold">We provide excellent customer service.</h5>
            </div>
        </div>
    </div>

    <div class="row py-5 justify-content-center">
        <div class="col-12">
            <div>
                <h3 class="responsive-text text-center libel-suit text-light-blue">our values</h3>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <p class="helvetica">We are in the people business. Our people are the most important asset we have. Our people put the
                        requirements and interests of our partners and the company ahead of individual gain. Below are the
                        values that our company adheres to:</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="row justify-content-lg-end">
                <div class="col-8">
                    <p class="mb-0 helvetica">W-inning Attitude</p>
                    <p class="mb-0 helvetica">E-ntrepreneurship</p>
                    <p class="mb-0 helvetica">H-onesty</p>
                    <p class="mb-0 helvetica">E-fficiency</p>
                    <p class="mb-0 helvetica">L-oyalty</p>
                    <p class="mb-0 helvetica">P-rofessionalism</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row our-guarantee py-5">
        <div class="col-12">
            <h3 class="responsive-text text-center libel-suit text-light-blue">our guarantee</h3>
            <p class="text-center helvetica">Rest assured that you are partnering with an expert packaging solutions provider and partner with a
                qualified and experienced team of competent and dedicated staff.</p>
            <div>
                <img class="img-fluid d-block mx-auto" src="{{ asset('images/our-guarantee.png') }}" alt="">
            </div>
        </div>
    </div>

    @include('partials.footer', ['inverted' => true])
@endsection