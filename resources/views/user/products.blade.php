@extends('layouts.app')

@section('content')
    <div class="row products-main">
        <div class="col">
            <div class="row h-100 align-items-center">
                <div class="col h-75 d-none d-lg-flex align-items-center left">

                </div>
                <div class="col h-75 d-flex align-items-center right">
                    <div class="w-lg-75">
                        <h1 class="responsive-text text-center text-lg-right libel-suit">
                            PRODUCTS
                        </h1>

                        <hr>

                        <p class="text-center text-lg-right helvetica">
                            Our products have pass the quality processes established thru our quality management system from sourcing of raw materials to its distribution.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-list-container">
        <div class="product-list">
            <div class="container-fluid p-0">
                <div class="row m-0">
                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES</h5>
                                <p class="mb-0 helvetica">SQUARE & DIAMOND</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">10 LITERS</p>
                                <p class="mb-0 helvetica">6 LITERS</p>
                                <p class="mb-0 helvetica">4 LITERS</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-1.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES</h5>
                                <p class="mb-0 helvetica">"GENERIC" (CLEAR)</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">1 LITER</p>
                                <p class="mb-0 helvetica">500 ML</p>
                                <p class="mb-0 helvetica">330 ML</p>
                                <p class="mb-0 helvetica">250 ML</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-2.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES</h5>
                                <p class="mb-0 helvetica">"GENERIC" (LIGHT BLUE)</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">1 LITER</p>
                                <p class="mb-0 helvetica">500 ML</p>
                                <p class="mb-0 helvetica">330 ML</p>
                                <p class="mb-0 helvetica">250 ML</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-3.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES</h5>
                                <p class="mb-0 helvetica">"PREMIUM"</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">SEXY BOTTLE</p>
                                <p class="mb-0 helvetica">CONE BOTTLE</p>
                                <p class="mb-0 helvetica">BULLET BOTTLE</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-4.jpg') }}">
                        </div>
                    </div>
                </div>

                <div class="row m-0">
                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES "PREMIUM"</h5>
                                <p class="mb-0 helvetica">SEXY BOTTLE</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">1 LITER</p>
                                <p class="mb-0 helvetica">500 ML</p>
                                <p class="mb-0 helvetica">330 ML</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-5.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES "PREMIUM"</h5>
                                <p class="mb-0 helvetica">BULLET BOTTLE</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">330 ML</p>
                                <p class="mb-0 helvetica">220 ML</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-6.jpg') }}">
                        </div>
                    </div>

                    {{-- <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLES "PREMIUM"</h5>
                                <p class="mb-0 helvetica">CONE BOTTLE</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">500 ML</p>
                                <p class="mb-0 helvetica">330 ML</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-7.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">5 GALLON SLIM JUG</h5>
                                <p class="mb-0 helvetica">ROTARY FAUCET</p>
                                <p class="mb-0 helvetica">PULL UP FAUCET</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">*Carry on handle</p>
                                <p class="mb-0 helvetica">*Unbreakable big cap for ease in cleaning</p>
                                <p class="mb-0 helvetica">*With faucet</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-8.jpg') }}">
                        </div>
                    </div> --}}

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper moved-to">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">5 GALLON SLIM JUG</h5>
                                <p class="mb-0 helvetica">ROTARY FAUCET</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">*Carry on handle</p>
                                <p class="mb-0 helvetica">*Unbreakable big cap for ease in cleaning</p>
                                <p class="mb-0 helvetica">*With faucet</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-9.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper moved-to">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON HALF ORDINARY BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON NON SPILL BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON FULL ORDINARY BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON FULL ORDINARY WHITE</h5>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-11.jpg') }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid p-0">
                <div class="row m-0">
                    {{-- <div class="col-6 col-lg-3 p-0 moved-from">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">5 GALLON SLIM JUG</h5>
                                <p class="mb-0 helvetica">ROTARY FAUCET</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">*Carry on handle</p>
                                <p class="mb-0 helvetica">*Unbreakable big cap for ease in cleaning</p>
                                <p class="mb-0 helvetica">*With faucet</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-9.jpg') }}">
                        </div>
                    </div> --}}

                    {{-- <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">5 GALLON SLIM JUG</h5>
                                <p class="mb-0 helvetica">PULL UP FAUCET</p>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">*Carry on handle</p>
                                <p class="mb-0 helvetica">*Unbreakable big cap for ease in cleaning</p>
                                <p class="mb-0 helvetica">*With faucet</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-10.jpg') }}">
                        </div>
                    </div> --}}

                    {{-- <div class="col-6 col-lg-3 p-0 moved-from">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON HALF ORDINARY BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON NON SPILL BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON FULL ORDINARY BLUE</h5>
                                <h5 class="helvetica-bold mb-0">CAP 5 GALLON FULL ORDINARY WHITE</h5>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                                <p class="mb-0 helvetica">54MM</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-11.jpg') }}">
                        </div>
                    </div> --}}

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">SEAPLUG</h5>
                                <h5 class="helvetica-bold mb-0">SLIM JUG BIG CAP</h5>
                                <h5 class="helvetica-bold mb-0">SMALL CAP</h5>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">113MM X 12 GRMS</p>
                                <p class="mb-0 helvetica">120MM X 25 GRMS SLIM JUG</p>
                                <p class="mb-0 helvetica">35MM X 3 GRMS</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-12.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper moved-to">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PREFORMS</h5>
                                <p class="mb-0 helvetica">(CLEAR & LIGHT BLUE)</p>
                            </div>

                            <div class="product-description">
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-15.jpg') }}">
                        </div>
                    </div>
                </div>

                <div class="row m-0">
                    {{-- <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLE CAPS</h5>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">26MM</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-13.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PET BOTTLE CAPS</h5>
                            </div>

                            <div class="product-description">
                                <p class="mb-0 helvetica">30MM</p>
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-14.jpg') }}">
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 p-0 position-relative product-wrapper">
                        <div>
                            <img class="product-image" src="https://via.placeholder.com/350x500" alt="">
                        </div>
                    </div> --}}

                    {{-- <div class="col-6 col-lg-3 p-0 moved-from">
                        <div class="position-absolute product-info p-4">
                            <div class="product-name mb-2">
                                <h5 class="helvetica-bold mb-0">PREFORMS</h5>
                                <p class="mb-0 helvetica">(CLEAR & LIGHT BLUE)</p>
                            </div>

                            <div class="product-description">
                            </div>
                        </div>

                        <div>
                            <img class="product-image" src="{{ asset('images/product-15.jpg') }}">
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection