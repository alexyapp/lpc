<div class="row footer {{ isset($invert) && $invert ? '' : 'text-white' }} {{ isset($invert) && $invert ? 'bg-white' : '' }}">
    <div class="col-12 col-md-6 d-flex align-items-center justify-content-center mt-5 left {{ isset($includeFooter) ? ($includeFooter ? '' : 'mb-5') : '' }}">
        <div>
            <div class="d-flex flex-column mb-4">
                <div class="d-flex align-items-center">
                    <div class="mr-3">
                        <img class="contact-icons" src="{{ asset('images/phone-icon.png') }}" alt="">
                    </div>
    
                    <div>
                        <p class="mb-0 helvetica {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">0917-867-4450</p>
    
                        <p class="mb-0 helvetica {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">0917-867-5639</p>
    
                        <p class="mb-0 helvetica {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">(032) 420-4399</p>
                    </div>
                </div>
            </div>
    
            <div class="d-flex flex-column mb-4">
                <div class="d-flex align-items-center">
                    <div class="mr-3">
                        <img class="contact-icons" src="{{ asset('images/email-icon.png') }}" alt="">
                    </div>
    
                    <div>
                        <p class="mb-0 helvetica {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">marketingofficercebu@lpc.com.ph</p>
                    </div>
                </div>
            </div>
    
            <div class="d-flex flex-column mb-4">
                <div class="d-flex align-items-center">
                    <div class="mr-3">
                        <img class="contact-icons" src="{{ asset('images/facebook-icon.png') }}" alt="">
                    </div>
    
                    <div>
                        <p class="helvetica mb-0 {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">Liquid Packaging Corporation</p>

                        <p class="helvetica mb-0 {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">LPC Sales</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6 right mt-5 map-responsive">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3924.9053931135345!2d123.95527471523408!3d10.349449492612111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a99815f1f2c8fb%3A0x881452605ac986c!2sLiquid%20Packaging%20Corporation!5e0!3m2!1sen!2sph!4v1614910614662!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>

    <div class="col-12 mt-5 {{ isset($includeFooter) ? ($includeFooter ? '' : 'd-none') : '' }}">
        @include('partials.footer-nav', ['invert' => isset($invert) ? $invert : null])
    </div>
</div>