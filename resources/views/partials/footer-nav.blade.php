<div class="d-md-flex align-items-center justify-content-center mb-3">
    <div>
        <a class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica" href="{{ route('user.about') }}">OUR COMPANY</a>
    </div>
    <span class="mx-3 d-none d-md-block {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">|</span>
    <div>
        <a class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica" href="{{ route('user.products') }}">PRODUCTS</a>
    </div>
    <span class="mx-3 d-none d-md-block {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">|</span>
    <div>
        <a class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica" href="{{ route('user.mission-vision') }}">MISSION AND VISION</a>
    </div>
    <span class="mx-3 d-none d-md-block {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">|</span>
    <div>
        <a class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica" href="{{ route('user.contact-us') }}">CONTACT US</a>
    </div>
    <span class="mx-3 d-none d-md-block {{ isset($invert) && $invert ? 'text-light-blue' : '' }}">|</span>
    <div>
        <a class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica" href="{{ route('user.events') }}">EVENTS</a>
    </div>
</div>

<div class="text-center mb-5">
    <p class="{{ isset($invert) && $invert ? 'text-light-blue' : 'text-white' }} helvetica">Liquid Packaging Corporation 2019</p>
</div>