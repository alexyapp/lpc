<nav class="navbar navbar-expand-md bg-white shadow-sm">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav w-100 justify-content-between align-items-center">
                <li class="nav-item">
                    <a href="{{ route('user.about') }}" class="nav-link helvetica-neue-medium text-light-blue">ABOUT US</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.products') }}" class="nav-link helvetica-neue-medium text-light-blue">PRODUCTS</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.mission-vision') }}" class="nav-link helvetica-neue-medium text-light-blue">MISSION AND VISION</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.home') }}" class="nav-link">
                        <img src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.contact-us') }}" class="nav-link helvetica-neue-medium text-light-blue">CONTACT US</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('user.faqs') }}" class="nav-link helvetica-neue-medium text-light-blue">FAQs</a>
                </li>

                {{-- <li class="nav-item">
                    <a href="{{ route('user.events') }}" class="nav-link helvetica-neue-medium text-light-blue">EVENTS</a>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>