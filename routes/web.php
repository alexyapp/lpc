<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

Route::view('/', 'user.home')->name('user.home');
Route::view('/about', 'user.about')->name('user.about');
Route::view('/products', 'user.products')->name('user.products');
Route::view('/mission-vision', 'user.mission-vision')->name('user.mission-vision');
Route::view('/contact-us', 'user.contact-us')->name('user.contact-us');
Route::view('/events', 'user.events')->name('user.events');
Route::view('/faqs', 'user.faqs')->name('user.faqs');
